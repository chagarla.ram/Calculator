public interface Operations {
    int compute(int a, int b);
}
